<?php
$cssdir = "CSS/";
$csssyles = "styles.css";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Lukas Hermansson</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href= <?php echo $cssdir . $csssyles; ?> rel = "stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
	<!--<link rel="icon" sizes="any" href="Images/faviocon.ico" type="image/x-icon">-->
	<link rel="icon" sizes="any" href="Images/faviocon.png" type="image/png">
	<link rel="icon" sizes="any" href="Images/Logo.svg" type="image/svg+xml">
</head>
<body>

  <?php
  $pagevar = "Pages";
  function GetPage() {
    if(isset($_GET["page"])){
      if(!empty($_GET["page"])){
          //We have a page var
          return $_GET["page"];
        }
        return null;
      }
      return null;
    }
		include "$pagevar/header.php";


    echo "<div class = 'container'>";
    echo "</div>";


		include "$pagevar/footer.php";

   ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="JS/functions.js"></script>
</body>
