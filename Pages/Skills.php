<section class="Skills" id="Skills">
  <h3>
    SKILLS
  </h3>
  <div class="SkillsIcons">
    <div class="SmallCircle">
      <?php include(dirname(__DIR__) . "/Images/icons/Pen.svg"); ?>
    </div>
    <div class="BigWrapper">
      <div class="BigCircle">
        <?php include(dirname(__DIR__) . "/Images/icons/App.svg"); ?>
      </div>
    </div>
    <div class="SmallCircle">
      <?php include( dirname(__DIR__) . "/Images/icons/Tech.svg"); ?>
    </div>
  </div>
  <span class="spacer"></span>
</section>
