<?php

$errors = array();

//Define actual strings to be gotten in post request
$NamePost = "Name";
$EmailPost = "Email";
$messagePost = "Message";

//Actual values
$name = null;
$email = null;
$message = null;

$requestsDone = null;
//Getting variables
if(isset($_POST[$NamePost])){
  if(!empty($_POST[$NamePost])){
    //We have a page var
    $name = $_POST[$NamePost];
  }
}
if(isset($_POST[$EmailPost])){
  if(!empty($_POST[$EmailPost])){
    //We have a page var
    $email = $_POST[$EmailPost];
  }
}
if(isset($_POST[$messagePost])){
  if(!empty($_POST[$messagePost])){
    //We have a page var
    $message = $_POST[$messagePost];
  }
}
$link = mysqli_connect("192.168.1.6", "root", "", "Web");

//Try to set up a connection
if (!$link) {
  $errors[] = "Database Error In connecting to DB";
}

//MYSql stage

$Ip  = $_SERVER['REMOTE_ADDR'];

if($result = $link->query("SELECT Count(*) FROM ContactAttempts WHERE IPAdress = INET_ATON('$Ip') AND Success = 1;")){
  $row = $result->fetch_assoc();

  $requestsDone = $row['Count(*)'];

}else{
  $errors[] = "Error in getting spamcheck response: " . $link->error;
}

//Validation stage

if($name != null){
  //No need to validate anything else for name
}else{
  $errors[] = "Name can't be empty";
}
if($email != null){
  //Validate email format
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors[] = "Invalid email format";
  }
}else{
  $errors[] = "Email can't be empty";
}
if($message != null){
  //no further validation for message
}else{
  $errors[] = "Message body can't be empty";
}


//Displaying stage
$response = new stdClass();
//Output is in JSON.
if(count($errors) > 0){
  //This might error if email is null as email field cant be null
  if($link->query("INSERT INTO ContactAttempts (IPAdress, Email, Success) VALUES (INET_ATON($Ip), $email, FALSE)")){

  }else{
    //This wont print becouse we are allready in the sucsess stage
    $errors[] = "Error posting failed response";
  }

  //We have an error lets display the first one.
  //Status of response
  $response->status = 0;
  //Message to be displayed
  $response->message = $errors[0];
  //Color for message to be displayed in

}else{

  if($link->query("INSERT INTO ContactAttempts (IPAdress, Email, Success) VALUES (INET_ATON($Ip), $email, TRUE)")){

  }else{
    //This wont print becouse we are allready in the sucsess stage
    $errors[] = "Error posting sucsessfull response";
  }

  //No error was found
  //Status for response
  $to = "lukas@lukashermansson.me";
  $subject = $name . " PageEmail";
  $txt = $message;
  $headers = "From: " . $email;

  mail($to,$subject,$txt,$headers);

  $response->status = 1;
  //Message to be dissplayed
  $response->message = "Success!";
  //Color for message to be displayed in.



}

//encode it to json ready for sending
$JsonResonse = json_encode($response);
//Echo resoults to page to be fetched with ajax
echo $JsonResonse;



?>
