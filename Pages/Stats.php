<section class="Stats" id="Stats">
  <h3>
    STATS
  </h3>
  <div class="StatsWrapper">

    <div>Java
      <span><em style="width: 90%;"></em></span>
    </div>
    <div>
      PHP
      <span><em style="width: 80%;"></em></span>
    </div>
    <div>
      HTML
      <span><em style="width: 50%;"></em></span>
    </div>
    <div>
      CSS
      <span><em style="width: 60%;"></em></span>
    </div>
    <div>
      C#
      <span><em style="width: 5%;"></em></span>
    </div>
    <div>
      General Tech
      <span><em style="width: 85%;"></em></span>
    </div>
  </div>
  <span class="spacer"></span>

</section>
