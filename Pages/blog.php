<?PHP
//How many posts are there on one page
$postsPerPage = 1;
//How many links do we want to show the user? the total amount
$NumActiveLinks = 4;
//Dont worrry closed account
$link = mysqli_connect("192.168.1.6", "root", "", "Web");

//Try to set up a connection
if (!$link) {
  echo "Error: Unable to connect to MySQL." . PHP_EOL;
  echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
  exit;
}
//Set charset
mysqli_set_charset($link, 'utf-8');
//Function to draw a page button
function DrawButton($number, $noLink){
  $e = $number+1;
  if($noLink){
    echo "<a>";
    //We add the class active to the one we are curently on
    echo "<div class='Button active'>" . $e . "</div>";
  }else{
    echo "<a href='http://lukashermansson.me/#Blog?pageNr=" . $number . "'>";
    echo "<div class='Button'>" . $e . "</div>";
  }
  if($noLink){
    echo "</a>";
  }else{
    echo "</a>";
  }
}
//Php code to get the page we are currently on from gt varable
function GetPage() {
  if(isset($_GET["pageNr"])){
    if(!empty($_GET["pageNr"])){
      //We have a page var
      return $_GET["pageNr"];
    }
    return 0;
  }
  return 0;
}
//Set a varable to the value of getPage()
$page = GetPage();
//Declare pageCount the number of pages we have
$pageCount = 0;

//SQL to set pageCount to the requred value
if($result = $link->query("SELECT Count(*) FROM BlogPosts")){
  $row = $result->fetch_assoc();

  $pageCount = ceil($row['Count(*)']/$postsPerPage);
}else{
  echo "Wrong SQL";
}
//Print some regular HTML
?>

<h3>BLOG</h3>

<p>Hello, and welcome to my personal area Here you can
read my blog updates and find links to my personal media.</p>




<?php
//sql to list posts on our current page this is determined in the limit statement
if($result = $link->query("SELECT * FROM BlogPosts ORDER BY postdate DESC LIMIT " . $page * $postsPerPage . ", " . $postsPerPage . ";")){
  //Loop all the rows and print that post with some layout html
  for($x = 0; $x < $result->num_rows; $x++){
		$row = $result->fetch_assoc();
    echo "<div class='post'>";
      echo "<div class='head'>";
        echo "<h4>" . $row['Title'] . "</h4>";
        echo "<span>" . $row['postdate'] . "</span>";
      echo "</div>";
      //Post content
      echo "<p>". $row['Content'] . "</p>";

      //Print spacer
      echo "<span class='spacer'></span>";
    echo "</div>";
  }
}else{
  echo mysqli_error($link);
}

//Lets not draw the pages if we can fit all on one page
if($pageCount < 0){
  //dont draw page buttons

}else {
  //we need the buttons
  echo "<div class='buttons'>";

  //Remaining varable to keep track of how many buttons we have added
  $remaining = $NumActiveLinks;
  //declaring 2 arraylists to be filled with postnumbers
  $linksUnder = array();
  $linksOver = array();
  //lets keep track of loopcount to detarmain what side we want to be on this loop iteration
  $LoopCount = 0;
  //offset to know how far from current page we are this loop
  $Offset = 1;
  //Loop until all elements are checked or until we have our buttons or we have looped untill Offset is bigger then our active links
  while($remaining > 0 && $Offset <= $NumActiveLinks){
    //Check if we are on an even loop or odd one
    if($LoopCount % 2 == 0){
      //even
      if($page + $Offset < $pageCount){
        //add element to upper links
        $linksOver[] = $page + $Offset;
        $remaining--;
      }
    }else{
      //Odd
      if($page - $Offset >= 0){
        //add element to under list
        $linksUnder[] = $page - $Offset;
        $remaining--;
      }
      //Only increse the offset if we are on the last try in loop
      $Offset++;
    }
    //increase loopcount
    $LoopCount++;

  }
  //reverse list becouse we are adding these in the oposite order that they are going to be displayed in
  $linksUnderReversed = array_reverse($linksUnder);
  //draw buttons
  //Lower buttons
  for($i = 0; $i < count($linksUnderReversed); $i++){
      DrawButton($linksUnderReversed[$i], false);
  }
  //Draw this page button
  DrawButton($page, true);
  //Upper buttons
  for($i = 0; $i < count($linksOver); $i++){
      DrawButton($linksOver[$i], false);
  }

  //close buttons div
  echo "</div>";
}

//Close SQL
mysqli_close($link);

?>
