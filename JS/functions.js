
//Page variable
var page = "#Home";

$(function(){
  //initialize smooth-scroll
  //Check if hash is empty
  if(!isEmpty(window.location.hash)){
    //we fill some variables with request information
    p = window.location.hash;
    page = p.split("?")[0];
  }
  //we load the page we got from prev if
  LoadPage(function () {
    AfterLoad();
  });
  //we initialize hash-change listener
  HashUpdate();

});

function AfterLoad(){
  smoothScrool(2000);
  InitSubmit();

}


function InitSubmit(){
    $("#ContactForm").submit(function ( event) {
       //Form is submitted craft request right here:
        event.preventDefault();

        var name = $("#name").val();
        var email = $("#email").val();
        var message = $("#message").val();

        var resp = $("#response");
        var submit = $("#submitBtn");

        var posting = $.post( "Pages/Mail.php", { Name: name, Email: email, Message: message } );

        posting.done(function( data ) {
          var res = JSON.parse(data);
          if(res.status == 1){
            submit.attr("disabled", true);
            resp.text("Sucsess!");
            resp.css("color", "green").show().fadeOut(3000);
          }else{
            resp.text(res.message);
            resp.css("color", "red").show().fadeOut(3000);
          }

        });

    });

}
//Loads page provides callback if you want to listen when its done
function LoadPage (callback){
  //Only 2 pages checking page variable
  if(page == "#Home"){
    //JQuery to replace everything in div with content of request
    $(".container").load("Pages/home.php", null, callback);
  }else if(page == "#Blog"){
    //JQuery to replace everything in div with content of request
    //Add other args provided to blog request
    $(".container").load("Pages/blog.php?" + window.location.hash.split("?")[1], null, callback);
  }
}
//SmoothScroll funtion originaly stolen from somewhere but is now nothing like the original
function smoothScrool (duration) {
  //register qlick listner
 	$(".scroll" ).on('click', function(event) {
    //Check if we are on apropiate part of the site to do the scroll
    if(page != "#Home"){
      //we are not on home
      page = "#Home";
      var denna = $(this);
      //Load page with the callback function bellow
      LoadPage(function(){
        //Scroll when loading of page is done
        DoScroll(denna, duration);
      });
    }else{
      //we are on home and can therefore just do the scrolling we dont need to concider any callbacks
      DoScroll($(this), duration);

    }
  });
}
//Function to do the scrolling of the site
function DoScroll(element, duration){
  //target gets filled with that special id so button has class in second classarray
  //that is put into target with #(Id sign) and is then loaded with Jquery
  var target = "#" + element.attr('class').split(' ')[1];
  //we animate the scrolling to the specified target
  $('html, body').animate({
   scrollTop: $(target).offset().top
 }, duration);
}

//Handles the hashcnageevent
function HashUpdate(){
  //Jquery event handling
  $(window).on('hashchange', function() {
    //we change these variables to the new value and then reload page
    p = window.location.hash;
    page = p.split("?")[0];
    //Loads the page with the new varables
    LoadPage();

  });
}
//function to check if string is empty
function isEmpty(str) {
    return (!str || 0 === str.length);
}

//function to handle buttonpresses this is called from html a tag
function ToggleMenu() {
    //JQuery select the 2 elemets we want to work with, h1 is suposed to get diferent styling too
    var h1 = $("h1" );
    //This is a div containing the links that are going to be restyled with this new class
    var x = $(".Links" );


    x.toggleClass("responsive");
    h1.toggleClass("responsive");

    //Previous code to handle swithcing classes
    /*if (!x.hasClass("responsive")) {
        x.addClass("responsive");
        h1.addClass("responsive");
    } else {
        x.removeClass("responsive");
        h1.removeClass("responsive");
    }*/

}
